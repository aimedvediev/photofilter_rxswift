//
//  ViewController.swift
//  PhotoFilter_RxSwift
//
//  Created by Anton Medvediev on 19/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var applyFilterButton: UIButton!
    @IBOutlet weak var photoImageView: UIImageView!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navC = segue.destination as? UINavigationController,
            let photosCVC = navC.viewControllers.first as? PhotoCollectionViewController else{
                fatalError("Segue destination not found")
        }
        photosCVC.selectedPhoto.subscribe(onNext: { [weak self] photo in
            DispatchQueue.main.async {
                self?.updateUI(with: photo)
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func applyFilterButtonPressed(){
        guard let sourceImage = self.photoImageView.image else{
            return
        }
        
        FiltersService().applyFilter(to: sourceImage).subscribe(onNext: { filteredImage in
            DispatchQueue.main.async {
                self.photoImageView.image = filteredImage
            }
        }).disposed(by: disposeBag)
        
    }
    
    private func updateUI(with image: UIImage){
        self.photoImageView.image = image
        self.applyFilterButton.isHidden = false
    }
    
}

