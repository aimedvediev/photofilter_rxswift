//
//  PhotoCollectionViewCell.swift
//  PhotoFilter_RxSwift
//
//  Created by Anton Medvediev on 20/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation
import UIKit

class PhotoCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var photoImageView: UIImageView!
}
